﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathsParser;
using System.Diagnostics.Contracts;

namespace SamPalmer.MathsParser.Core.Tests
{
	[TestClass]
	public class EvaluationTests
	{
		[TestMethod]
		public void ConstantsShouldBeParsed()
		{
			const int value = 0;
			var expression = value.ToString();

			Test(expression, value);
		}

		private void Test(string expression, int value)
		{
			var parser = new Parser(expression);
			var result = parser.Evaluate();
			Assert.AreEqual(value, result, "Failure for expression: {0}.", expression);
		}
		
		[TestMethod]
		public void NullValuesShouldBeRejected()
		{
			Test<ArgumentNullException>(null);
		}

		[TestMethod]
		public void NumbersPrefixedWithZeroShouldBeTreatedAsDecimal()
		{
			const int number = 10;
			Test("0" + number.ToString(), number);
		}

		[TestMethod]
		public void NumbersPrefixedWithZeroExShouldNotBeConsideredValid()
		{
			Test<FormatException>("0x13");
		}

		[TestMethod]
		public void AdditionShouldWork()
		{
			Test(1, '+', 2, add);
		}

		[TestMethod]
		public void FullTest()
		{
			Test("(((1 +2) / 3)*4)-(-(+3))", 7);
		}

		private static int add(int a, int b)
		{
			return a + b;
		}

		private void Test(int firstOperand, char @operator, int secondOperand, Func<int, int, int> calculateExpectedResult)
		{
			var expression = String.Format("{0}{1}{2}", firstOperand, @operator, secondOperand);
			Test(expression, calculateExpectedResult(firstOperand, secondOperand));
		}

		[TestMethod]
		public void MultiplicationShouldWork()
		{
			Test(5, '*', 3, multiply);
		}

		private static int multiply(int a, int b)
		{
			return a * b;
		}

		private void Test<ExceptionType>(string expression)
			where ExceptionType : Exception
		{
			try
			{
				new Parser(expression).Evaluate();
			}
			catch (ExceptionType)
			{
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using SamPalmer.Common;
using System.Text.RegularExpressions;

namespace MathsParser
{
	public class Parser
	{
		private readonly List<Token> tokens;
		private int currentTokenIndex = 0;

		public Parser(string expression)
		{
			if (expression == null)
				throw new ArgumentNullException();

			var tokeniser = new Tokeniser(expression);
			tokens = tokeniser.Tokens;
		}

		private Token CurrentToken
		{
			get
			{
				return GetToken(currentTokenIndex);
			}
		}

		private Token GetToken(int index)
		{
			if (index >= tokens.Count)
				return Token.Null;

			return tokens[index];
		}

		private Token ReadToken()
		{
			return GetToken(MoveToNextToken());
		}

		private int MoveToNextToken()
		{
			return currentTokenIndex++;
		}

		public Expression Parse()
		{
			currentTokenIndex = 0;
			var result = Expression();

			if (result != null && currentTokenIndex != tokens.Count)
				throw new FormatException("Unexpected end of expression.");

			return result;
		}

		private Expression Expression()
		{
			return (Expression)BinaryExpression() ?? (Expression)UnaryExpression() ?? Literal();
		}

		private Literal Literal()
		{
			int value;

			if (CurrentToken.Type == TokenType.Integer && int.TryParse(CurrentToken.Value, out value))
			{
				MoveToNextToken();
				return new Literal(value);
			}
			else
				return null;
		}

		private TokenPositionTracker TrackTokenPosition()
		{
			return new TokenPositionTracker(currentTokenIndex, position => currentTokenIndex = position);
		}

		private BinaryExpression BinaryExpression()
		{
			using (var tracker = TrackTokenPosition())
			{
				var firstOperand = Subexpression();
				var @operator = BinaryOperator();
				var secondOperand = Subexpression();

				BinaryExpression result = null;

				if (!new object[] { firstOperand, @operator, secondOperand }.Contains(null))
					result = new BinaryExpression(firstOperand, @operator, secondOperand);

				return tracker.Return(result);
			}
		}

		private Expression Subexpression()
		{
			return BracketedExpression() ?? (Expression)Literal();
		}

		private Expression BracketedExpression()
		{
			using (var tracker = TrackTokenPosition())
			{
				if (CurrentToken.Type == TokenType.Bracket && CurrentToken.Value == "(")
				{
					MoveToNextToken();
					var result = Expression();

					if (CurrentToken.Type == TokenType.Bracket && CurrentToken.Value == ")")
					{
						MoveToNextToken();
						return tracker.Return(result);
					}
				}

				return tracker.Return<Expression>(null);
			}
		}

		private UnaryExpression UnaryExpression()
		{
			using (var tracker = TrackTokenPosition())
			{
				var @operator = UnaryOperator();
				var operand = Subexpression();

				UnaryExpression result = null;

				if (!new object[] { @operator, operand }.Contains(null))
					result = new UnaryExpression(@operator, operand);

				return tracker.Return(result);
			}
		}

		private BinaryOperator BinaryOperator()
		{
			BinaryOperator result = null;

			if (CurrentToken.Type == TokenType.Operator)
			{
				switch (CurrentToken.Value)
				{
					case "+":
						result = new Addition();
						break;
					case "-":
						result = new Subtraction();
						break;
					case "*":
						result = new Multiplication();
						break;
					case "/":
						result = new Division();
						break;
				}

				if (result != null)
					MoveToNextToken();
			}

			return result;
		}

		private UnaryOperator UnaryOperator()
		{
			UnaryOperator result = null;

			if (CurrentToken.Type == TokenType.Operator)
			{
				switch (CurrentToken.Value)
				{
					case "+":
						result = new Positive();
						break;
					case "-":
						result = new Negation();
						break;
				}

				if (result != null)
					MoveToNextToken();
			}

			return result;
		}

		public int Evaluate()
		{
			var expression = Parse();
			if (expression == null)
				throw new FormatException();

			return expression.Evaluate();
		}
	}
}

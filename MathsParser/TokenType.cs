using System;
using System.Collections.Generic;
using System.Linq;

namespace MathsParser
{
	class TokenType
	{
		public static TokenType Null = new TokenType();
		public static TokenType Integer = new TokenType(Char.IsNumber);
		public static TokenType Operator = new TokenType('+', '-', '*', '/');
		public static TokenType Bracket = new TokenType('(', ')');

		private static IEnumerable<TokenType> All
		{
			get
			{
				return new[]
					{
						Integer,
						Operator,
						Bracket,
					};
			}
		}

		public static TokenType Identify(char character)
		{
			var type = All.SingleOrDefault(tokenType => tokenType.isMatching(character));
			if (type.IsDefaultForType())
				return Null;

			return type;
		}

		private Func<char, bool> isMatching;

		private TokenType(params char[] validCharacters)
			: this(character => validCharacters.Contains(character))
		{
			SingleCharacter = true;
		}

		private TokenType(Func<char, bool> isMatching)
		{
			this.isMatching = isMatching;
		}

		public bool IsMatching(char character)
		{
			return isMatching(character);
		}

		public bool SingleCharacter { get; private set; }
	}
}

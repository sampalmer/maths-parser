using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using SamPalmer.Common;
using System.Text.RegularExpressions;

namespace MathsParser
{
	class Token
	{
		public TokenType Type { get; set; }
		public string Value { get; set; }

		public static readonly Token Null = new Token
		{
			Value = string.Empty,
			Type = TokenType.Null,
		};
	}
}

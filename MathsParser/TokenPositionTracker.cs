﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathsParser
{
	class TokenPositionTracker : IDisposable
	{
		private readonly int originalTokenPosition;
		private readonly Action<int> setTokenPosition;
		private bool disposed;
		private bool parsingFailed;
		private bool expressionReturned;

		public TokenPositionTracker(int originalTokenPosition, Action<int> setTokenPosition)
		{
			this.originalTokenPosition = originalTokenPosition;
			this.setTokenPosition = setTokenPosition;
		}

		public void Dispose()
		{
			if (disposed)
				throw new InvalidOperationException("Already disposed.");

			if (!expressionReturned)
				throw new InvalidOperationException("Disposed without first returning an expression result.");

			if (parsingFailed)
				RestoreTokenPosition();

			disposed = true;
		}

		private void RestoreTokenPosition()
		{
			setTokenPosition(originalTokenPosition);
		}

		public T Return<T>(T expression)
			where T : class
		{
			parsingFailed = expression == null;
			expressionReturned = true;
			return expression;
		}
	}
}

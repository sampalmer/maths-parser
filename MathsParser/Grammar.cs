﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
	Expression = BinaryExpression | UnaryExpression | Literal
	Literal = [0-9]
	BinaryExpression = Subexpression BinaryOperator Subexpression
	UnaryExpression = UnaryOperator Subexpression
	Subexpression = "(" Expression ")" | Literal
	UnaryOperator = [+-]
	BinaryOperator = [+-/*]
*/

namespace MathsParser
{
	public interface Expression
	{
		int Evaluate();
	}

	public class Literal : Expression
	{
		public Literal(int value)
		{
			Value = value;
		}

		public int Value { get; private set; }

		public int Evaluate()
		{
			return Value;
		}
	}

	public class BinaryExpression : Expression
	{
		public BinaryExpression(Expression firstOperand, BinaryOperator @operator, Expression secondOperand)
		{
			FirstOperand = firstOperand;
			Operator = @operator;
			SecondOperand = secondOperand;
		}

		public Expression FirstOperand { get; private set; }
		public BinaryOperator Operator { get; private set; }
		public Expression SecondOperand { get; private set; }

		public int Evaluate()
		{
			var firstOperandValue = FirstOperand.Evaluate();
			var secondOperandValue = SecondOperand.Evaluate();
			return Operator.Evaluate(firstOperandValue, secondOperandValue);
		}
	}

	public class UnaryExpression : Expression
	{
		public UnaryExpression(UnaryOperator @operator, Expression operand)
		{
			Operator = @operator;
			Operand = operand;
		}

		public UnaryOperator Operator { get; private set; }
		public Expression Operand { get; set; }

		public int Evaluate()
		{
			var operandValue = Operand.Evaluate();
			return Operator.Evaluate(operandValue);
		}
	}

	public interface UnaryOperator
	{
		int Evaluate(int operand);
	}

	public class Positive : UnaryOperator
	{
		public int Evaluate(int operand)
		{
			return operand;
		}
	}

	public class Negation : UnaryOperator
	{
		public int Evaluate(int operand)
		{
			return -operand;
		}
	}

	public interface BinaryOperator
	{
		int Evaluate(int firstOperand, int secondOperand);
	}

	public class Addition : BinaryOperator
	{
		public int Evaluate(int firstOperand, int secondOperand)
		{
			return firstOperand + secondOperand;
		}
	}

	public class Subtraction : BinaryOperator
	{
		public int Evaluate(int firstOperand, int secondOperand)
		{
			return firstOperand - secondOperand;
		}
	}

	public class Multiplication : BinaryOperator
	{
		public int Evaluate(int firstOperand, int secondOperand)
		{
			return firstOperand * secondOperand;
		}
	}

	public class Division : BinaryOperator
	{
		public int Evaluate(int firstOperand, int secondOperand)
		{
			return firstOperand / secondOperand;
		}
	}
}

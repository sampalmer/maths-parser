using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using SamPalmer.Common;
using System.Text.RegularExpressions;

namespace MathsParser
{
	class Tokeniser
	{
		private class TokenInProgress
		{
			private StringBuilder value;
			private TokenType type;

			public TokenInProgress(char character, TokenType type)
			{
				value = new StringBuilder(character.ToString());
				this.type = type;
			}

			internal void Append(char character)
			{
				value.Append(character);
			}

			internal Token Build()
			{
				return new Token
				{
					Type = type,
					Value = value.ToString()
				};
			}

			internal bool CanAppend(char character)
			{
				return type.IsMatching(character) && !type.SingleCharacter;
			}
		}

		public List<Token> Tokens { get; private set; }
		private TokenInProgress currentToken = null;

		public Tokeniser(string expression)
		{
			Tokens = new List<Token>();

			foreach (var character in expression)
				if (InToken)
					if (Char.IsWhiteSpace(character))
						AddToken();
					else if (currentToken.CanAppend(character))
						currentToken.Append(character);
					else
						BeginToken(character);
				else if (!Char.IsWhiteSpace(character))
					BeginToken(character);

			if (InToken)
				AddToken();
		}

		private void BeginToken(char character)
		{
			if (InToken)
				AddToken();

			var type = TokenType.Identify(character);
			if (type == TokenType.Null)
				throw new FormatException("Unexpected character: " + character);

			currentToken = new TokenInProgress(character, type);
		}

		private void AddToken()
		{
			Tokens.Add(currentToken.Build());
			currentToken = null;
		}

		private bool InToken
		{
			get
			{
				return currentToken != null;
			}
		}
	}
}
